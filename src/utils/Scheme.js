// 方案类文件
class Scheme {
    // 子节点数据结构
    subScheme={
        name:"子方案a", // name
        id:"1000001",
        selectFlag:false,
        editFlag:false,
        form: {...this.defaultForm}, //子方案选项详细
        grandScheme:[]
    }
    // 孙子节点数据结构
    grandScheme={
        name:"奖金项",
        id:"100000201",
        changeNameFlag: false,
        short:[], //入围条件
        calculate:[], // 计算逻辑
        // 注意：孙子节点的选中不在selectFlag判断，在 selectedGrand 中判断
    }
    // 入围条件节点
    short = {
        name:"入围条件一",
        id:"100000301",
        changeNameFlag: false,
        form: { ...this.defaultShortForm }, //入围条件选项详细
    }
    // 计算逻辑节点
    calculate = {
        name:"计算条件一",
        id:"100000401",
        changeNameFlag: false
    }
    data={
        name:"方案1", // name
        subScheme:[{
            ...this.subScheme,
            grandScheme:[{...this.grandScheme}]
        }]
    }
    // 选中的子项目
    selectedSub = null
    // 选中的孙子项目
    selectedGrand = null

    //子方案详细条件
    defaultForm = { 
        planValue: [],
        rewardValue: "",
        chanel: "",
        institution:'',
        planTime:'',
        planObject: '',
        settlement:'',
    }
    //入围条件选项详细
    defaultShortForm = {
        investigationObject:'', //考察对象
        inputModel: '2', //输入方式
        type: '',//配置条件
        time: '',//日期
        tableHeader: ''
    }
}

export default new Scheme()