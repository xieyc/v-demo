import signDate from "@/components/templates/signDate"
import calculateItem from "@/components/templates/calculateItem"
import quick from "@/components/templates/quick"

class ENUM {
    // 右侧短列表推荐
    INDEX_TAG = [
        { template:signDate, label: "当月FYC", id: 1015, englishName:'dyFYC' },
        { template:signDate, label: "特定金额", id: 1016, englishName:'TDmoney' },
        { template:signDate, label: "推荐奖比例", id: 1017, englishName:'tjreward' },
        { template:signDate, label: "辅导将比例", id: 1001, englishName:'qyrq' },
        { template:signDate, label: "转正补发比例", id: 1002, englishName:'qyrq' },
        { template:signDate, label: "考核期（月数）", id: 1003, englishName:'qyrq' },
        { template:signDate, label: "考核指标", id: 1004, englishName:'qyrq' },

        { template:signDate, label: "是否正式员工", id: 1005, englishName:'qyrq' },
        { template:signDate, label: "月均FYC", id: 1006, englishName:'cql'  },
        // { template:signDate, label: "健康人力", id: 1003, englishName:'jkrl'  },
        { template:signDate, label: "发放月数", id: 1007 , englishName:'ryzt' },
        { template:signDate, label: "绩优人力", id: 1008 , englishName:'zyrl' },
        // { template:signDate, label: "衔训结训", id: 1006, englishName:'xxjx'  },
        { template:signDate, label: "岗前结训", id: 1009 , englishName:'gqpx' },
        { template:signDate, label: "年龄", id: 1010 , englishName:'nl' },
        { template:signDate, label: "档次", id: 1011 , englishName:'jssj' },
        { template:signDate, label: "学历", id: 1012 , englishName:'jssj' },

        // { template:signDate, label: "健康/绩优/双绩优人力标准", id: 1010 , englishName:'jk/jy' },
        // { template:signDate, label: "城市", id: 1011 , englishName:'city' },
        // { template:signDate, label: "基本法类别", id: 1012, englishName:'basicType'  },
        // { template:signDate, label: "AMS积分", id: 1013 , englishName:'jf' },
        // { template:signDate, label: "EPA状态", id: 1014, englishName:'zt'  },
        
    ]
    // 右侧计算工具箱
    CAL_BOX = [
        { template:quick, icon:"icon1",label: "快捷赋值", id: 3001 },
        { template:calculateItem, icon:"icon2",label: "档位表", id: 3002 },
        { template:calculateItem, icon:"icon3",label: "1+N架构", id: 3003 },
        { template:calculateItem, icon:"icon4",label: "连续", id: 3004 },
        { template:calculateItem, icon:"icon5",label: "累计", id: 3005 },
        { template:calculateItem, icon:"icon6",label: "排序", id: 3006 },
    ]
    // 方案类型
    SCHEME_TYPE = [
        {
            label: "人力发展", id: 2001, children: [
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200101 },
                { label: "增员推荐类",subLabel:'(增员活动支持）', id: 200102 },
                { label: "新人成长类",subLabel:'(常规FNA)', id: 200103 },
                { label: "新人成长类",subLabel:'(追加FNA)', id: 200104 },
                { label: "准主管晋升类",subLabel:'(主管队伍建设)', id: 200105 },
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200106 },
                { label: "团队成长类",subLabel:'(产能队伍建设)', id: 200107 },

            ]
        },
        {
            label: "业务推动", id: 2002, children: [
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200201 },
                { label: "增员推荐类",subLabel:'(增员活动支持）', id: 200302 },
                { label: "新人成长类",subLabel:'(常规FNA)', id: 200203 },
                { label: "新人成长类",subLabel:'(追加FNA)', id: 200204 },
                { label: "准主管晋升类",subLabel:'(主管队伍建设)', id: 200205 },
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200206 },
                { label: "团队成长类",subLabel:'(产能队伍建设)', id: 200207 },
            ]
        },
        {
            label: "客户经营", id: 2003, children: [
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200301 },
                { label: "增员推荐类",subLabel:'(增员活动支持）', id: 200302 },
                { label: "新人成长类",subLabel:'(常规FNA)', id: 200303 },
                { label: "新人成长类",subLabel:'(追加FNA)', id: 200304 },
                { label: "准主管晋升类",subLabel:'(主管队伍建设)', id: 200305 },
                { label: "增员推荐类",subLabel:'(增员推动奖励）', id: 200306 },
                { label: "团队成长类",subLabel:'(产能队伍建设)', id: 200307 },
            ]
        },
    ]
    // 方案对象
    SCHEME_OBJECT = [
        {
            label: "全部", id: 3001, children: [
                { label: "全部",id: 300101 },
                { label: "室主任",id: 300102 },
                { label: "部经理",id: 300103 },
                { label: "总监",id: 300104 },
            ]
        },
        {
            label: "业务推动", id: 3002, children: [
            ]
        },
        {
            label: "客户经营", id: 3003, children: [
            ]
        },
    ]

    //档数维度选择框
    options = [
        {label: 'A类', value:'0'},
        {label: 'B类', value:'1'},
        {label: 'C类', value:'2'},
        {label: 'D类', value:'3'},
    ]

    //
    options1 = [
        {label: '绩效择优标准', value:'0'},
        {label: '标准要求二档', value:'1'},
        {label: '折算等级', value:'2'},
        {label: '级别不同类型', value:'3'},
    ]

}

export default new ENUM()