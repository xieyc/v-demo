class BASIC {
  PLAY_TYPE = [{
    value: '1',
    label: '新人成长类'
  }, {
    value: '2',
    label: '新人晋升类'
  }, {
    value: '3',
    label: '增员推荐类'
  }, {
    value: '4',
    label: '团队成长类'
  }, {
    value: '5',
    label: '准主管晋升类'
  }]
}
export default new BASIC()

//奖励类型
export const REWARD_TYPE = [
  {
    label: "人力发展", value: 2001, children: [
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200101 },
      { label: "增员推荐类", subLabel: '(增员活动支持）', value: 200102 },
      { label: "新人成长类", subLabel: '(常规FNA)', value: 200103 },
      { label: "新人成长类", subLabel: '(追加FNA)', value: 200104 },
      { label: "准主管晋升类", subLabel: '(主管队伍建设)', value: 200105 },
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200106 },
      { label: "团队成长类", subLabel: '(产能队伍建设)', value: 200107 },

    ]
  },
  {
    label: "业务推动", value: 2002, children: [
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200201 },
      { label: "增员推荐类", subLabel: '(增员活动支持）', value: 200302 },
      { label: "新人成长类", subLabel: '(常规FNA)', value: 200203 },
      { label: "新人成长类", subLabel: '(追加FNA)', value: 200204 },
      { label: "准主管晋升类", subLabel: '(主管队伍建设)', value: 200205 },
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200206 },
      { label: "团队成长类", subLabel: '(产能队伍建设)', value: 200207 },
    ]
  },
  {
    label: "客户经营", value: 2003, children: [
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200301 },
      { label: "增员推荐类", subLabel: '(增员活动支持）', value: 200302 },
      { label: "新人成长类", subLabel: '(常规FNA)', value: 200303 },
      { label: "新人成长类", subLabel: '(追加FNA)', value: 200304 },
      { label: "准主管晋升类", subLabel: '(主管队伍建设)', value: 200305 },
      { label: "增员推荐类", subLabel: '(增员推动奖励）', value: 200306 },
      { label: "团队成长类", subLabel: '(产能队伍建设)', value: 200307 },
    ]
  },
]

//渠道
export const CHANNEL = [
  {
    value: '1',
    label: '个险'
  }, {
    value: '2',
    label: '商险'
  }, {
    value: '5',
    label: '团险'
  }
]
//机构institution
export const INSTITUTION = [
  {
    value: '1',
    label: '上海营业区'
  }, {
    value: '2',
    label: '上海直辖部'
  }, {
    value: '3',
    label: '上海'
  }, {
    value: '4',
    label: '广州'
  }, {
    value: '5',
    label: '深圳'
  }
]
//方案对象
export const PLAY_OBJECT = [
  {
    label: "全部", value: 3001, children: [
      { label: "全部", value: 300101 },
      { label: "室主任", value: 300102 },
      { label: "部经理", value: 300103 },
      { label: "总监", value: 300104 },
    ]
  },
  {
    label: "试用业务员", value: 3002, children: [
      { label: "全部", value: 300101 },
      { label: "室主任", value: 300102 },
      { label: "部经理", value: 300103 },
      { label: "总监", value: 300104 },
    ]
  },
  {
    label: "正式业务员", value: 3003, children: [
      { label: "全部", value: 300101 },
      { label: "室主任", value: 300102 },
      { label: "部经理", value: 300103 },
      { label: "总监", value: 300104 },
    ]
  },
  {
    label: '专务', value: 3004, children: [
      { label: "全部", value: 300101 },
      { label: "室主任", value: 300102 },
      { label: "部经理", value: 300103 },
      { label: "总监", value: 300104 },
    ]
  },
  {
    label: '主管', value: 3005, children: [
      { label: "全部", value: 300101 },
      { label: "室主任", value: 300102 },
      { label: "部经理", value: 300103 },
      { label: "总监", value: 300104 },
    ]
  }
]
// 结算方式
export const SETTLEMENT = [
  {
    value: '1',
    label: '月结'
  }, {
    value: '2',
    label: '季度结'
  }, {
    value: '3',
    label: '年结'
  }
]
//编辑公式选项条件
export const CHOOSE_ARR = [
  {name:'CE財补档位', id:'1'},
  {name:'出勤条件1', id:'2'},
  {name:'出勤条件2', id:'3'},
  {name:'出勤条件3', id:'4'},
  {name:'出勤条件4', id:'5'},
  {name:'出勤条件5', id:'6'},
  {name:'拜访条件', id:'7'},
  {name:'培训条件1', id:'8'},
  {name:'培训条件2', id:'9'},
  {name:'举绩条件1', id:'10'},
  {name:'举绩条件2', id:'11'},
]
//公式
export const FORMULA = [
  {name: '+', id:'a' },
  {name: '-', id:'a'},
  {name: 'x', id:'a'},
  {name: '÷', id:'a'},
  {name: '(', id:'a'},
  {name: ')', id:'a'},
  {name: '||', id:'a'},
  {name: '&', id:'a'},
  {name: '!', id:'a'},

]


