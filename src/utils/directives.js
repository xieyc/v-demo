export const autoLine = {
    inserted: (el, binding, vnode) => {
        // 遍历所有节点
      let subDom = document.getElementsByClassName("sub-tree-item")
      let grandDom = document.getElementsByClassName("grand-tree-item")
      let lastDom1 = document.getElementById("box1")?.getElementsByClassName("last-tree-item")
      let lastDom2 = document.getElementById("box2")?.getElementsByClassName("last-tree-item")
      let distence = "9px"
      Array.from(subDom).forEach(el=>{
          //console.log("ele",el);
          setDom(el)
      })
      Array.from(grandDom).forEach((el,index)=>{
          //console.log("ele",el);
        //   if(index === grandDom.length - 1){
        //       el.setAttribute("isLast","1")
        //   }
          setDom(el)
      })
      if(el._prevClass === "last-tree-item"){
        distence = "14px"
        Array.from(lastDom1).forEach((el,index)=>{
            //console.log("ele",el);
            // if(index === lastDom.length - 1){
            //     el.setAttribute("isLast","1")
            // }
            setDom(el)
        })
        Array.from(lastDom2).forEach((el,index)=>{
            //console.log("ele",el);
            // if(index === lastDom.length - 1){
            //     el.setAttribute("isLast","1")
            // }
            setDom(el)
        })
      }
      function setDom(el){
          //console.log("---", el.className, binding, vnode);
          // 获取上个元素
          const preNodeHeight =
          el.previousSibling
          && el.previousSibling._prevClass === el._prevClass
              ? el.previousSibling.clientHeight
              : 0; // height
          //console.log("pre ele:", preNodeHeight);
          el.setAttribute(
          "data-line",
          preNodeHeight+ "px"
          );
          // 创建元素 - 先确认是否存在这个节点
          const lineDom = el.getElementsByClassName("line")
          if(lineDom.length == 0){
              let dom = document.createElement("div");
              dom.setAttribute("class","line");
              dom.style.position = "absolute";
              dom.style.height = preNodeHeight + 40 + "px";
              dom.style.width = "2px";
              dom.style.background = "#1964DF";
              dom.style.left = "-15px";
              dom.style.top = distence;
              dom.style.transform = "translate(0,-100%)";
              // 第一个元素
              if (!preNodeHeight){
              dom.style.height = preNodeHeight + 20 + "px";
              }
              // 最后一个元素
              if (!el.nextSibling || el.nextSibling._prevClass !== el._prevClass){
                  dom.style.background = "rgb(218, 218, 218)";
              }
              el.appendChild(dom);
          } else {
              lineDom[0].style.height =!preNodeHeight ?  preNodeHeight + 20 + "px" : preNodeHeight + 40 + "px";
          }
          
      }
    },
  }